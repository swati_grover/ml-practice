#!/bin/bash

sudo yum update
sudo yum remove postgresql*
sudo yum install postgresql-server postgresql-contrib postgresql-devel postgresql-libs postgresql
#sudo postgresql-setup initdb
#sudo systemctl start postgresql
#sudo systemctl enable postgresql
#systemctl status postgresql

yum -y install python34 \
        python34-devel \
        python34-pip \
        gcc \
        nginx

pip3 install --upgrade pip
pip3 install virtualenv requests \
         cmd2  \
         beautifulsoup4 \
	 Flask \
	 flask-restful \
	 uwsgi



#export LC_ALL=en_US.utf-8
#export LANG=en_US.utf-8


#sudo -u postgres psql postgres
#\password postgres
#CREATE TABLE COMPANY(ID INT PRIMARY KEY     NOT NULL,NAME           TEXT    NOT NULL,AGE INT NOT NULL,ADDRESS CHAR(50),SALARY REAL, JOIN_DATE DATE);
#INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY,JOIN_DATE) VALUES (1, 'Paul', 32, 'California', 20000.00,'2001-07-13');


#sudo netstat -plunt |grep postgres


#uwsgi --socket 0.0.0.0:8000 --protocol=http -w wsgi
#uwsgi --ini myproject.ini

#https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-centos-7



#wget http://www.kaggle.com/c/instacart-market-basket-analysis/data/orders.csv

CREATE TABLE products (product_id integer,product_name varchar(1000),aisle_id integer,department_id integer,PRIMARY KEY(product_id));
CREATE VIEW products_view AS SELECT * FROM products;


CREATE TABLE department (department_id integer,department varchar(1000));
CREATE VIEW department_view AS SELECT * FROM department;


CREATE TABLE orders (order_id integer,user_id integer ,eval_set varchar(1000), order_number integer,order_dow integer,order_hour_of_day integer, days_since_prior_order decimal(7,2));
CREATE VIEW orders_view AS SELECT * FROM orders;


CREATE TABLE order_products__prior (order_id integer,product_id integer, add_to_cart_order integer, reordered integer);
CREATE VIEW order_products__prior_view AS SELECT * FROM order_products__prior;


CREATE TABLE order_products__train (order_id integer,product_id integer, add_to_cart_order integer, reordered integer);
CREATE VIEW order_products__train_view AS SELECT * FROM order_products__train;


CREATE TABLE aisles (aisle_id integer,aisle varchar(1000));
CREATE VIEW aisles_view AS SELECT * FROM aisles;


CREATE TABLE users (username varchar(20) PRIMARY KEY, password varchar(100), email varchar(50), settings varchar(32500), tracking varchar(32500), rank integer);

COPY products FROM '/tmp/products.csv' CSV HEADER;
COPY department FROM '/tmp/departments.csv' CSV HEADER;
COPY orders FROM '/tmp/orders.csv' CSV HEADER;
COPY order_products__prior FROM '/tmp/order_products__prior.csv' CSV HEADER;
COPY order_products__train FROM '/tmp/order_products__train.csv' CSV HEADER;
COPY aisles FROM '/tmp/aisles.csv' CSV HEADER;
DROP TABLE users;

*/15 * * * * sudo echo 1 > /proc/sys/vm/drop_caches



######################################################################################################################################################################
Stored procedure
######################################################################################################################################################################

\df+ procname 

________________________________________________________

1.)DELETE
_________________________________________________________

Create table department_backup(department_id integer, department varchar(70) , date timestamp DEFAULT NOW());
Create table department_log(department_id integer , date timestamp DEFAULT NOW()); 

CREATE OR REPLACE FUNCTION delete_department(deletedepartment VARCHAR(70))
    RETURNS void AS $$
    BEGIN
                  insert into department_backup (department_id, department) select department_id, department from department where department = deletedepartment;     
                  delete from department where department = deletedepartment;
    END;
    $$ LANGUAGE plpgsql;

select delete_department('test');


_________________________________________________________

2.)ADD

_________________________________________________________



***********
Trigger
*********

CREATE TRIGGER insert_trigger AFTER INSERT ON department
FOR EACH ROW EXECUTE PROCEDURE logfunc();
-------
CREATE OR REPLACE FUNCTION logfunc() RETURNS TRIGGER AS $example_table$
   BEGIN
      INSERT INTO department_log(department_id, date) VALUES (new.department_id, current_timestamp);
      RETURN NEW;
   END;
$example_table$ LANGUAGE plpgsql;

-----
INSERT INTO department VALUES (22, 'dummy');

_________________________________________________________


3.)UPDATE
_________________________________________________________



CREATE OR REPLACE FUNCTION update_department(currentdept VARCHAR(70), newdept VARCHAR(70))
    RETURNS void AS $$
    BEGIN
                  insert into department_backup (department_id, department) select department_id, department from department where department = currentdept; 
                  update department set department = newdept where department = currentdept;
    END;
    $$ LANGUAGE plpgsql;

select update_department('test', 'test2');
	
################################################################################################################################




Part 2




create or replace view order_product as
Select a.order_id, a.product_id, a.add_to_cart_order, a.reordered, b.product_name, b.department_id, b.aisle_id
from order_products__train a, products b
where a.product_id = b.product_id;

create or replace view order_dept as
Select a.order_id, a.product_id, a.add_to_cart_order, a.reordered, a.product_name, b.department, a.aisle_id, b.department_id
From order_product a, department b
where a.department_id = b.department_id;

create or replace view order_aisle as
Select a.order_id, a.product_id, a.add_to_cart_order, a.reordered, a.product_name, a.department, b.aisle, a.aisle_id, a.department_id
from order_dept a, aisles b
where a.aisle_id = b.aisle_id;

create or replace view order_product_department_aisle_vw as
Select a.order_id, a.product_id, a.add_to_cart_order, a.reordered, a.product_name, a.department, a.aisle, a.aisle_id, a.department_id, b.user_id, b.eval_set, b.order_number, b.order_dow, b.order_hour_of_day, b.days_since_prior_order from order_aisle a, orders b where a.order_id = b.order_id;


CREATE EXTENSION tablefunc WITH SCHEMA public;




create or replace view top1000products as Select product_id from (Select count(user_id) totalusers, product_id from order_product_department_aisle_vw group by product_id order by 1 desc limit 1000) as foo;



create or replace view top1000users as Select user_id from (Select user_id, count(b.product_id) product_count from order_product_department_aisle_vw a, top1000products b where a.product_id = b.product_id group by user_id order by 1 desc limit 100 ) as foo;



create or replace view tb8 as Select a.user_id::text ,a.product_id::integer, count(a.product_id)::integer as test from order_product_department_aisle_vw a,top1000products b, top1000users c where a.product_id = b.product_id and a.user_id = c.user_id group by a.user_id,a.product_id order by 1,2;



create or replace view top1000producttop100user_VW as SELECT * FROM crosstab( 'select user_id, product_id, test from tb8 order by 1,2', 'SELECT distinct(product_id) from tb8 order by 1' ) AS final_result(user_id TEXT, P1 int,

P2 int,

P3 int,

P4 int,

P5 int,

P6 int,

P7 int,

P8 int,

P9 int,

P10 int,

P11 int,

P12 int,

P13 int,

P14 int,

P15 int,

P16 int,

P17 int,

P18 int,

P19 int,

P20 int,

P21 int,

P22 int,

P23 int,

P24 int,

P25 int,

P26 int,

P27 int,

P28 int,

P29 int,

P30 int,

P31 int,

P32 int,

P33 int,

P34 int,

P35 int,

P36 int,

P37 int,

P38 int,

P39 int,

P40 int,

P41 int,

P42 int,

P43 int,

P44 int,

P45 int,

P46 int,

P47 int,

P48 int,

P49 int,

P50 int,

P51 int,

P52 int,

P53 int,

P54 int,

P55 int,

P56 int,

P57 int,

P58 int,

P59 int,

P60 int,

P61 int,

P62 int,

P63 int,

P64 int,

P65 int,

P66 int,

P67 int,

P68 int,

P69 int,

P70 int,

P71 int,

P72 int,

P73 int,

P74 int,

P75 int,

P76 int,

P77 int,

P78 int,

P79 int,

P80 int,

P81 int,

P82 int,

P83 int,

P84 int,

P85 int,

P86 int,

P87 int,

P88 int,

P89 int,

P90 int,

P91 int,

P92 int,

P93 int,

P94 int,

P95 int,

P96 int,

P97 int,

P98 int,

P99 int,

P100 int,

P101 int,

P102 int,

P103 int,

P104 int,

P105 int,

P106 int,

P107 int,

P108 int,

P109 int,

P110 int,

P111 int,

P112 int,

P113 int,

P114 int,

P115 int,

P116 int,

P117 int,

P118 int,

P119 int,

P120 int,

P121 int,

P122 int,

P123 int,

P124 int,

P125 int,

P126 int,

P127 int,

P128 int,

P129 int,

P130 int,

P131 int,

P132 int,

P133 int,

P134 int,

P135 int,

P136 int,

P137 int,

P138 int,

P139 int,

P140 int,

P141 int,

P142 int,

P143 int,

P144 int,

P145 int,

P146 int,

P147 int,

P148 int,

P149 int,

P150 int,

P151 int,

P152 int,

P153 int,

P154 int,

P155 int,

P156 int,

P157 int,

P158 int,

P159 int,

P160 int,

P161 int,

P162 int,

P163 int,

P164 int,

P165 int,

P166 int,

P167 int,

P168 int,

P169 int,

P170 int,

P171 int,

P172 int,

P173 int,

P174 int,

P175 int,

P176 int,

P177 int,

P178 int,

P179 int,

P180 int,

P181 int,

P182 int,

P183 int,

P184 int,

P185 int,

P186 int,

P187 int,

P188 int,

P189 int,

P190 int,

P191 int,

P192 int,

P193 int,

P194 int,

P195 int,

P196 int,

P197 int,

P198 int,

P199 int,

P200 int,

P201 int,

P202 int,

P203 int,

P204 int,

P205 int,

P206 int,

P207 int,

P208 int,

P209 int,

P210 int,

P211 int,

P212 int,

P213 int,

P214 int,

P215 int,

P216 int,

P217 int,

P218 int,

P219 int,

P220 int,

P221 int,

P222 int,

P223 int,

P224 int,

P225 int,

P226 int,

P227 int,

P228 int,

P229 int,

P230 int,

P231 int,

P232 int,

P233 int,

P234 int,

P235 int,

P236 int,

P237 int,

P238 int,

P239 int,

P240 int,

P241 int,

P242 int,

P243 int,

P244 int,

P245 int,

P246 int,

P247 int,

P248 int,

P249 int,

P250 int,

P251 int,

P252 int,

P253 int,

P254 int,

P255 int,

P256 int,

P257 int,

P258 int,

P259 int,

P260 int,

P261 int,

P262 int,

P263 int,

P264 int,

P265 int,

P266 int,

P267 int,

P268 int,

P269 int,

P270 int,

P271 int,

P272 int,

P273 int,

P274 int,

P275 int,

P276 int,

P277 int,

P278 int,

P279 int,

P280 int,

P281 int,

P282 int,

P283 int,

P284 int,

P285 int,

P286 int,

P287 int,

P288 int,

P289 int,

P290 int,

P291 int,

P292 int,

P293 int,

P294 int,

P295 int,

P296 int,

P297 int,

P298 int,

P299 int,

P300 int,

P301 int,

P302 int,

P303 int,

P304 int,

P305 int,

P306 int,

P307 int,

P308 int,

P309 int,

P310 int,

P311 int,

P312 int,

P313 int,

P314 int,

P315 int,

P316 int,

P317 int,

P318 int,

P319 int,

P320 int,

P321 int,

P322 int,

P323 int,

P324 int,

P325 int,

P326 int,

P327 int,

P328 int,

P329 int,

P330 int,

P331 int,

P332 int,

P333 int,

P334 int,

P335 int,

P336 int,

P337 int,

P338 int,

P339 int,

P340 int,

P341 int,

P342 int,

P343 int,

P344 int,

P345 int,

P346 int,

P347 int,

P348 int,

P349 int,

P350 int,

P351 int,

P352 int,

P353 int,

P354 int,

P355 int,

P356 int,

P357 int,

P358 int,

P359 int,

P360 int,

p361 int);



create or replace view tb9 as Select a.user_id::text ,a.product_id::integer, count(a.product_id)::integer as test from order_product_department_aisle_vw a,top1000products b, top1000users c where a.product_id = b.product_id and a.user_id = c.user_id group by a.user_id,a.product_id order by 1,2;


create table top1000producttop100user as select user_id,

coalesce(P1 , 0) AS P1 ,

coalesce(P2 , 0) AS P2 ,

coalesce(P3 , 0) AS P3 ,

coalesce(P4 , 0) AS P4 ,

coalesce(P5 , 0) AS P5 ,

coalesce(P6 , 0) AS P6 ,

coalesce(P7 , 0) AS P7 ,

coalesce(P8 , 0) AS P8 ,

coalesce(P9 , 0) AS P9 ,

coalesce(P10 , 0) AS P10  ,

coalesce(P11 , 0) AS P11  ,

coalesce(P12 , 0) AS P12  ,

coalesce(P13 , 0) AS P13  ,

coalesce(P14 , 0) AS P14  ,

coalesce(P15 , 0) AS P15  ,

coalesce(P16 , 0) AS P16  ,

coalesce(P17 , 0) AS P17  ,

coalesce(P18 , 0) AS P18  ,

coalesce(P19 , 0) AS P19  ,

coalesce(P20 , 0) AS P20  ,

coalesce(P21 , 0) AS P21  ,

coalesce(P22 , 0) AS P22  ,

coalesce(P23 , 0) AS P23  ,

coalesce(P24 , 0) AS P24  ,

coalesce(P25 , 0) AS P25  ,

coalesce(P26 , 0) AS P26  ,

coalesce(P27 , 0) AS P27  ,

coalesce(P28 , 0) AS P28  ,

coalesce(P29 , 0) AS P29  ,

coalesce(P30 , 0) AS P30  ,

coalesce(P31 , 0) AS P31  ,

coalesce(P32 , 0) AS P32  ,

coalesce(P33 , 0) AS P33  ,

coalesce(P34 , 0) AS P34  ,

coalesce(P35 , 0) AS P35  ,

coalesce(P36 , 0) AS P36  ,

coalesce(P37 , 0) AS P37  ,

coalesce(P38 , 0) AS P38  ,

coalesce(P39 , 0) AS P39  ,

coalesce(P40 , 0) AS P40  ,

coalesce(P41 , 0) AS P41  ,

coalesce(P42 , 0) AS P42  ,

coalesce(P43 , 0) AS P43  ,

coalesce(P44 , 0) AS P44  ,

coalesce(P45 , 0) AS P45  ,

coalesce(P46 , 0) AS P46  ,

coalesce(P47 , 0) AS P47  ,

coalesce(P48 , 0) AS P48  ,

coalesce(P49 , 0) AS P49  ,

coalesce(P50 , 0) AS P50  ,

coalesce(P51 , 0) AS P51  ,

coalesce(P52 , 0) AS P52  ,

coalesce(P53 , 0) AS P53  ,

coalesce(P54 , 0) AS P54  ,

coalesce(P55 , 0) AS P55  ,

coalesce(P56 , 0) AS P56  ,

coalesce(P57 , 0) AS P57  ,

coalesce(P58 , 0) AS P58  ,

coalesce(P59 , 0) AS P59  ,

coalesce(P60 , 0) AS P60  ,

coalesce(P61 , 0) AS P61  ,

coalesce(P62 , 0) AS P62  ,

coalesce(P63 , 0) AS P63  ,

coalesce(P64 , 0) AS P64  ,

coalesce(P65 , 0) AS P65  ,

coalesce(P66 , 0) AS P66  ,

coalesce(P67 , 0) AS P67  ,

coalesce(P68 , 0) AS P68  ,

coalesce(P69 , 0) AS P69  ,

coalesce(P70 , 0) AS P70  ,

coalesce(P71 , 0) AS P71  ,

coalesce(P72 , 0) AS P72  ,

coalesce(P73 , 0) AS P73  ,

coalesce(P74 , 0) AS P74  ,

coalesce(P75 , 0) AS P75  ,

coalesce(P76 , 0) AS P76  ,

coalesce(P77 , 0) AS P77  ,

coalesce(P78 , 0) AS P78  ,

coalesce(P79 , 0) AS P79  ,

coalesce(P80 , 0) AS P80  ,

coalesce(P81 , 0) AS P81  ,

coalesce(P82 , 0) AS P82  ,

coalesce(P83 , 0) AS P83  ,

coalesce(P84 , 0) AS P84  ,

coalesce(P85 , 0) AS P85  ,

coalesce(P86 , 0) AS P86  ,

coalesce(P87 , 0) AS P87  ,

coalesce(P88 , 0) AS P88  ,

coalesce(P89 , 0) AS P89  ,

coalesce(P90 , 0) AS P90  ,

coalesce(P91 , 0) AS P91  ,

coalesce(P92 , 0) AS P92  ,

coalesce(P93 , 0) AS P93  ,

coalesce(P94 , 0) AS P94  ,

coalesce(P95 , 0) AS P95  ,

coalesce(P96 , 0) AS P96  ,

coalesce(P97 , 0) AS P97  ,

coalesce(P98 , 0) AS P98  ,

coalesce(P99 , 0) AS P99  ,

coalesce(P100 , 0) AS P100 ,

coalesce(P101 , 0) AS P101 ,

coalesce(P102 , 0) AS P102 ,

coalesce(P103 , 0) AS P103 ,

coalesce(P104 , 0) AS P104 ,

coalesce(P105 , 0) AS P105 ,

coalesce(P106 , 0) AS P106 ,

coalesce(P107 , 0) AS P107 ,

coalesce(P108 , 0) AS P108 ,

coalesce(P109 , 0) AS P109 ,

coalesce(P110 , 0) AS P110 ,

coalesce(P111 , 0) AS P111 ,

coalesce(P112 , 0) AS P112 ,

coalesce(P113 , 0) AS P113 ,

coalesce(P114 , 0) AS P114 ,

coalesce(P115 , 0) AS P115 ,

coalesce(P116 , 0) AS P116 ,

coalesce(P117 , 0) AS P117 ,

coalesce(P118 , 0) AS P118 ,

coalesce(P119 , 0) AS P119 ,

coalesce(P120 , 0) AS P120 ,

coalesce(P121 , 0) AS P121 ,

coalesce(P122 , 0) AS P122 ,

coalesce(P123 , 0) AS P123 ,

coalesce(P124 , 0) AS P124 ,

coalesce(P125 , 0) AS P125 ,

coalesce(P126 , 0) AS P126 ,

coalesce(P127 , 0) AS P127 ,

coalesce(P128 , 0) AS P128 ,

coalesce(P129 , 0) AS P129 ,

coalesce(P130 , 0) AS P130 ,

coalesce(P131 , 0) AS P131 ,

coalesce(P132 , 0) AS P132 ,

coalesce(P133 , 0) AS P133 ,

coalesce(P134 , 0) AS P134 ,

coalesce(P135 , 0) AS P135 ,

coalesce(P136 , 0) AS P136 ,

coalesce(P137 , 0) AS P137 ,

coalesce(P138 , 0) AS P138 ,

coalesce(P139 , 0) AS P139 ,

coalesce(P140 , 0) AS P140 ,

coalesce(P141 , 0) AS P141 ,

coalesce(P142 , 0) AS P142 ,

coalesce(P143 , 0) AS P143 ,

coalesce(P144 , 0) AS P144 ,

coalesce(P145 , 0) AS P145 ,

coalesce(P146 , 0) AS P146 ,

coalesce(P147 , 0) AS P147 ,

coalesce(P148 , 0) AS P148 ,

coalesce(P149 , 0) AS P149 ,

coalesce(P150 , 0) AS P150 ,

coalesce(P151 , 0) AS P151 ,

coalesce(P152 , 0) AS P152 ,

coalesce(P153 , 0) AS P153 ,

coalesce(P154 , 0) AS P154 ,

coalesce(P155 , 0) AS P155 ,

coalesce(P156 , 0) AS P156 ,

coalesce(P157 , 0) AS P157 ,

coalesce(P158 , 0) AS P158 ,

coalesce(P159 , 0) AS P159 ,

coalesce(P160 , 0) AS P160 ,

coalesce(P161 , 0) AS P161 ,

coalesce(P162 , 0) AS P162 ,

coalesce(P163 , 0) AS P163 ,

coalesce(P164 , 0) AS P164 ,

coalesce(P165 , 0) AS P165 ,

coalesce(P166 , 0) AS P166 ,

coalesce(P167 , 0) AS P167 ,

coalesce(P168 , 0) AS P168 ,

coalesce(P169 , 0) AS P169 ,

coalesce(P170 , 0) AS P170 ,

coalesce(P171 , 0) AS P171 ,

coalesce(P172 , 0) AS P172 ,

coalesce(P173 , 0) AS P173 ,

coalesce(P174 , 0) AS P174 ,

coalesce(P175 , 0) AS P175 ,

coalesce(P176 , 0) AS P176 ,

coalesce(P177 , 0) AS P177 ,

coalesce(P178 , 0) AS P178 ,

coalesce(P179 , 0) AS P179 ,

coalesce(P180 , 0) AS P180 ,

coalesce(P181 , 0) AS P181 ,

coalesce(P182 , 0) AS P182 ,

coalesce(P183 , 0) AS P183 ,

coalesce(P184 , 0) AS P184 ,

coalesce(P185 , 0) AS P185 ,

coalesce(P186 , 0) AS P186 ,

coalesce(P187 , 0) AS P187 ,

coalesce(P188 , 0) AS P188 ,

coalesce(P189 , 0) AS P189 ,

coalesce(P190 , 0) AS P190 ,

coalesce(P191 , 0) AS P191 ,

coalesce(P192 , 0) AS P192 ,

coalesce(P193 , 0) AS P193 ,

coalesce(P194 , 0) AS P194 ,

coalesce(P195 , 0) AS P195 ,

coalesce(P196 , 0) AS P196 ,

coalesce(P197 , 0) AS P197 ,

coalesce(P198 , 0) AS P198 ,

coalesce(P199 , 0) AS P199 ,

coalesce(P200 , 0) AS P200 ,

coalesce(P201 , 0) AS P201 ,

coalesce(P202 , 0) AS P202 ,

coalesce(P203 , 0) AS P203 ,

coalesce(P204 , 0) AS P204 ,

coalesce(P205 , 0) AS P205 ,

coalesce(P206 , 0) AS P206 ,

coalesce(P207 , 0) AS P207 ,

coalesce(P208 , 0) AS P208 ,

coalesce(P209 , 0) AS P209 ,

coalesce(P210 , 0) AS P210 ,

coalesce(P211 , 0) AS P211 ,

coalesce(P212 , 0) AS P212 ,

coalesce(P213 , 0) AS P213 ,

coalesce(P214 , 0) AS P214 ,

coalesce(P215 , 0) AS P215 ,

coalesce(P216 , 0) AS P216 ,

coalesce(P217 , 0) AS P217 ,

coalesce(P218 , 0) AS P218 ,

coalesce(P219 , 0) AS P219 ,

coalesce(P220 , 0) AS P220 ,

coalesce(P221 , 0) AS P221 ,

coalesce(P222 , 0) AS P222 ,

coalesce(P223 , 0) AS P223 ,

coalesce(P224 , 0) AS P224 ,

coalesce(P225 , 0) AS P225 ,

coalesce(P226 , 0) AS P226 ,

coalesce(P227 , 0) AS P227 ,

coalesce(P228 , 0) AS P228 ,

coalesce(P229 , 0) AS P229 ,

coalesce(P230 , 0) AS P230 ,

coalesce(P231 , 0) AS P231 ,

coalesce(P232 , 0) AS P232 ,

coalesce(P233 , 0) AS P233 ,

coalesce(P234 , 0) AS P234 ,

coalesce(P235 , 0) AS P235 ,

coalesce(P236 , 0) AS P236 ,

coalesce(P237 , 0) AS P237 ,

coalesce(P238 , 0) AS P238 ,

coalesce(P239 , 0) AS P239 ,

coalesce(P240 , 0) AS P240 ,

coalesce(P241 , 0) AS P241 ,

coalesce(P242 , 0) AS P242 ,

coalesce(P243 , 0) AS P243 ,

coalesce(P244 , 0) AS P244 ,

coalesce(P245 , 0) AS P245 ,

coalesce(P246 , 0) AS P246 ,

coalesce(P247 , 0) AS P247 ,

coalesce(P248 , 0) AS P248 ,

coalesce(P249 , 0) AS P249 ,

coalesce(P250 , 0) AS P250 ,

coalesce(P251 , 0) AS P251 ,

coalesce(P252 , 0) AS P252 ,

coalesce(P253 , 0) AS P253 ,

coalesce(P254 , 0) AS P254 ,

coalesce(P255 , 0) AS P255 ,

coalesce(P256 , 0) AS P256 ,

coalesce(P257 , 0) AS P257 ,

coalesce(P258 , 0) AS P258 ,

coalesce(P259 , 0) AS P259 ,

coalesce(P260 , 0) AS P260 ,

coalesce(P261 , 0) AS P261 ,

coalesce(P262 , 0) AS P262 ,

coalesce(P263 , 0) AS P263 ,

coalesce(P264 , 0) AS P264 ,

coalesce(P265 , 0) AS P265 ,

coalesce(P266 , 0) AS P266 ,

coalesce(P267 , 0) AS P267 ,

coalesce(P268 , 0) AS P268 ,

coalesce(P269 , 0) AS P269 ,

coalesce(P270 , 0) AS P270 ,

coalesce(P271 , 0) AS P271 ,

coalesce(P272 , 0) AS P272 ,

coalesce(P273 , 0) AS P273 ,

coalesce(P274 , 0) AS P274 ,

coalesce(P275 , 0) AS P275 ,

coalesce(P276 , 0) AS P276 ,

coalesce(P277 , 0) AS P277 ,

coalesce(P278 , 0) AS P278 ,

coalesce(P279 , 0) AS P279 ,

coalesce(P280 , 0) AS P280 ,

coalesce(P281 , 0) AS P281 ,

coalesce(P282 , 0) AS P282 ,

coalesce(P283 , 0) AS P283 ,

coalesce(P284 , 0) AS P284 ,

coalesce(P285 , 0) AS P285 ,

coalesce(P286 , 0) AS P286 ,

coalesce(P287 , 0) AS P287 ,

coalesce(P288 , 0) AS P288 ,

coalesce(P289 , 0) AS P289 ,

coalesce(P290 , 0) AS P290 ,

coalesce(P291 , 0) AS P291 ,

coalesce(P292 , 0) AS P292 ,

coalesce(P293 , 0) AS P293 ,

coalesce(P294 , 0) AS P294 ,

coalesce(P295 , 0) AS P295 ,

coalesce(P296 , 0) AS P296 ,

coalesce(P297 , 0) AS P297 ,

coalesce(P298 , 0) AS P298 ,

coalesce(P299 , 0) AS P299 ,

coalesce(P300 , 0) AS P300 ,

coalesce(P301 , 0) AS P301 ,

coalesce(P302 , 0) AS P302 ,

coalesce(P303 , 0) AS P303 ,

coalesce(P304 , 0) AS P304 ,

coalesce(P305 , 0) AS P305 ,

coalesce(P306 , 0) AS P306 ,

coalesce(P307 , 0) AS P307 ,

coalesce(P308 , 0) AS P308 ,

coalesce(P309 , 0) AS P309 ,

coalesce(P310 , 0) AS P310 ,

coalesce(P311 , 0) AS P311 ,

coalesce(P312 , 0) AS P312 ,

coalesce(P313 , 0) AS P313 ,

coalesce(P314 , 0) AS P314 ,

coalesce(P315 , 0) AS P315 ,

coalesce(P316 , 0) AS P316 ,

coalesce(P317 , 0) AS P317 ,

coalesce(P318 , 0) AS P318 ,

coalesce(P319 , 0) AS P319 ,

coalesce(P320 , 0) AS P320 ,

coalesce(P321 , 0) AS P321 ,

coalesce(P322 , 0) AS P322 ,

coalesce(P323 , 0) AS P323 ,

coalesce(P324 , 0) AS P324 ,

coalesce(P325 , 0) AS P325 ,

coalesce(P326 , 0) AS P326 ,

coalesce(P327 , 0) AS P327 ,

coalesce(P328 , 0) AS P328 ,

coalesce(P329 , 0) AS P329 ,

coalesce(P330 , 0) AS P330 ,

coalesce(P331 , 0) AS P331 ,

coalesce(P332 , 0) AS P332 ,

coalesce(P333 , 0) AS P333 ,

coalesce(P334 , 0) AS P334 ,

coalesce(P335 , 0) AS P335 ,

coalesce(P336 , 0) AS P336 ,

coalesce(P337 , 0) AS P337 ,

coalesce(P338 , 0) AS P338 ,

coalesce(P339 , 0) AS P339 ,

coalesce(P340 , 0) AS P340 ,

coalesce(P341 , 0) AS P341 ,

coalesce(P342 , 0) AS P342 ,

coalesce(P343 , 0) AS P343 ,

coalesce(P344 , 0) AS P344 ,

coalesce(P345 , 0) AS P345 ,

coalesce(P346 , 0) AS P346 ,

coalesce(P347 , 0) AS P347 ,

coalesce(P348 , 0) AS P348 ,

coalesce(P349 , 0) AS P349 ,

coalesce(P350 , 0) AS P350 ,

coalesce(P351 , 0) AS P351 ,

coalesce(P352 , 0) AS P352 ,

coalesce(P353 , 0) AS P353 ,

coalesce(P354 , 0) AS P354 ,

coalesce(P355 , 0) AS P355 ,

coalesce(P356 , 0) AS P356 ,

coalesce(P357 , 0) AS P357 ,

coalesce(P358 , 0) AS P358 ,

coalesce(P359 , 0) AS P359 ,

coalesce(P360 , 0) AS P360 ,

coalesce(P361 , 0) AS P361
From top1000producttop100user_VW;   


******************************************************************************
CREATE VIEW tb8 AS
    SELECT (a.user_id)::text AS user_id, a.product_id, (count(a.product_id))::integer AS test, a.product_name FROM order_product_department_aisle_vw a, top1000products b, top1000users c WHERE ((a.product_id = b.product_id) AND (a.user_id = c.user_id)) GROUP BY a.user_id, a.product_id, a.product_name ORDER BY (a.user_id)::text, a.product_id;




create or replace view product_col_map as 
select product_id, product_name, concat('p'::text, rn::text) as column_cd
from
(
Select product_id, product_name, ROW_NUMBER() over (order by product_id desc) as rn 
from (SELECT distinct(product_id) as product_id, product_name from tb8) as foo1
) as foo;
